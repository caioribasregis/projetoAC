package com.avenuecode.service;

import java.util.ArrayList;
import java.util.List;

import com.avenuecode.dto.DistanceDTO;
import com.avenuecode.dto.DistanciaNumberDTO;
import com.avenuecode.dto.InnerRouteDTO;
import com.avenuecode.model.Graph;
import com.avenuecode.model.Route;

public class DistanceService {

	private Graph graph = null;
	private String town1 = null;
	private String town2 = null;
	private Integer maxStops = null;

	
	
	public DistanceService() {
	}

	public DistanciaNumberDTO getDistanceNumber(DistanceDTO distance, Graph graph) throws Exception {
		String source;
		String target;
		Integer cont = 1;
		Integer distanceNumber = 0;
		DistanciaNumberDTO dto = new DistanciaNumberDTO();

		// busca proximos source e targer
		for (int i = 0; i < distance.getPath().size(); i++) {
			if (i + 1 < distance.getPath().size()) {
				source = distance.getPath().get(i);
				target = distance.getPath().get(i + 1);

				for (Route route : graph.getData()) {

					// se a source e target for as informadas add a distanciaNumber em uma lista
					if (route.getSource().equals(source) && route.getTarget().equals(target)) {
						distanceNumber = distanceNumber + route.getDistance();
						cont++;
					}
				}
			}
		}
		// retorna NOT FOUND caso o caminho informado n�o exista
		if (cont < distance.getPath().size()) {
			throw new Exception(" NOT FOUND ");
		}
		dto.setDistance(distanceNumber);
		return dto;
	}

	// ROTA MAIS CURTA
	public InnerRouteDTO getShortestRoute(Graph pGraph, String pTown1, String pTown2) {
		town1 = pTown1;
		town2 = pTown2;
		graph = pGraph;

		List<InnerRouteDTO> listRoutes = new ArrayList<InnerRouteDTO>();

		if (town1.equals(town2)) {
			return new InnerRouteDTO(town1, 0);
		}

		// verifica se � a cidade informada, cria o dto e add na lista caso for
		for (Route route : graph.getData()) {
			if (route.getSource().equals(town1)) {
				InnerRouteDTO innerRoute = availableRoutesDistance(route, 0);
				if (innerRoute != null) {
					innerRoute.route = route.getSource() + innerRoute.route;
					listRoutes.add(innerRoute);
				}
			}
		}

		// verifica e busca caminho mais curto para retorno
		InnerRouteDTO shortest = null;
		for (InnerRouteDTO route : listRoutes) {
			if (shortest == null) {
				shortest = route;
			} else {
				if (route.stops < shortest.stops) {
					shortest = route;
				}
			}
		}
		return shortest;
	}

	// Metodo recursivo
	private InnerRouteDTO availableRoutesDistance(Route route, Integer stops) {

		// cria dto caso target for a town2
		if (route.getTarget().equals(town2)) {
			return new InnerRouteDTO(route.getTarget(), 0);
		} else {
			for (Route route2 : graph.getData()) {

				// busca o proximo target caso for diferente da anterior e nao for a town1
				if (route2.getSource().equals(route.getTarget()) && !route2.getTarget().equals(route.getSource())
						&& !route2.getTarget().equals(town1)) {

					// Cria dto com nome da rota e stops para retorno
					InnerRouteDTO innerRoute = availableRoutesDistance(route2, stops + 1);
					if (innerRoute != null) {
						innerRoute.route = route2.getSource() + innerRoute.route;
						innerRoute.stops += route.getDistance();
					} else {
						new InnerRouteDTO(route.getTarget(), 0);
					}
					return innerRoute;
				}
			}
			return null;
		}
	}

	public String getTown1() {
		return town1;
	}

	public void setTown1(String town1) {
		this.town1 = town1;
	}

	public String getTown2() {
		return town2;
	}

	public void setTown2(String town2) {
		this.town2 = town2;
	}

	public Integer getMaxStops() {
		return maxStops;
	}

	public void setMaxStops(Integer maxStops) {
		this.maxStops = maxStops;
	}

}
