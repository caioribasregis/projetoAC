package com.avenuecode.service;

import java.util.ArrayList;
import java.util.List;

import com.avenuecode.dto.InnerRouteDTO;
import com.avenuecode.model.Graph;
import com.avenuecode.model.Route;

public class RouteService {

	private Graph graph = null;
	private String town1 = null;
	private String town2 = null;
	private Integer maxStops = null;
	
	
	public List<InnerRouteDTO> getAvaiableRoutes(Graph pGraph, String pTown1, String pTown2, Integer pStops) {
		town1 = pTown1;
		town2 = pTown2;
		maxStops = pStops;
		graph = pGraph;
		List<InnerRouteDTO> listRoutes = new ArrayList<InnerRouteDTO>();

		//busca a lista das rotas da source passada
		for (Route route : graph.getData()) {
			if (route.getSource().equals(town1)) {
				List<InnerRouteDTO> ret = availableRoutesStops(route, 1);
				for (InnerRouteDTO innerRoute : ret) {
					innerRoute.route = route.getSource() + innerRoute.route;
					listRoutes.add(innerRoute);
				}
			}
		}
		return listRoutes;
	}

	
	
	private List<InnerRouteDTO> availableRoutesStops(Route route, Integer stops) {
		
		//verifica o maxStops
		if (stops > maxStops) {
			return new ArrayList<>();
		} else {
			
			//add a rota caso for a cidade desejada
			if (route.getTarget().equals(town2)) {
				List<InnerRouteDTO> ret = new ArrayList<InnerRouteDTO>();
				ret.add(new InnerRouteDTO(route.getTarget(), stops));
				return ret;
			} else {
				
				//senao verifica na lista se � a rota e add
				List<InnerRouteDTO> ret = new ArrayList<>();
				for (Route route2 : graph.getData()) {
					if (route2.getSource().equals(route.getTarget())) {
						List<InnerRouteDTO> innerRoutes = availableRoutesStops(route2, stops + 1);
						for (InnerRouteDTO innerRoute : innerRoutes) {
							innerRoute.route = route2.getSource() + innerRoute.route;
							ret.add(innerRoute);
						}
					}
				}
				return ret;
			}
		}
	}

}
