package com.avenuecode.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.avenuecode.model.Graph;

@Repository
public interface GraphRepository extends CrudRepository<Graph, Long> {

	
	
	
}
