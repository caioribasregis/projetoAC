package com.avenuecode.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.avenuecode.model.Route;

@Repository
public interface RouteRepository extends CrudRepository<Route, Long> {

}
