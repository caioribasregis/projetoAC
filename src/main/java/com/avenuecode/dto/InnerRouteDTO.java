package com.avenuecode.dto;

public class InnerRouteDTO {

	public String route;
	public Integer stops;

	public InnerRouteDTO(String route, Integer stops) {
		this.route = route;
		this.stops = stops;
	}
	
	public InnerRouteDTO() {
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public Integer getStops() {
		return stops;
	}

	public void setStops(Integer stops) {
		this.stops = stops;
	}

	
}
