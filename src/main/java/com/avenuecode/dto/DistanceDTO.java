package com.avenuecode.dto;

import java.util.List;

public class DistanceDTO {

	private List<String> path;

	public List<String> getPath() {
		return path;
	}

	public void setPath(List<String> path) {
		this.path = path;
	}
}
