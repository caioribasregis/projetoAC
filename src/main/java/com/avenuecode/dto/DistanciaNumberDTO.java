package com.avenuecode.dto;

public class DistanciaNumberDTO {
	
	private Integer distance;

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}
	
}
