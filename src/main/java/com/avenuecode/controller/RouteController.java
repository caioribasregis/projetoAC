package com.avenuecode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.dto.InnerRouteDTO;
import com.avenuecode.model.Graph;
import com.avenuecode.repository.GraphRepository;
import com.avenuecode.service.RouteService;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/routes")
public class RouteController {

	@Autowired
	private GraphRepository repository;
	private RouteService service = new RouteService();

	
	//3.Find available routes from a given pair of towns on saved graph 
	@PostMapping("/{id}/from/{town1}/to/{town2}")
    public List<InnerRouteDTO> getRouteList(@RequestParam Integer maxStops, @PathVariable Long id, @PathVariable String town1,
            @PathVariable String town2) {
        Graph graph = repository.findOne(id);
		return service.getAvaiableRoutes(graph, town1, town2, maxStops);

    }

	

}
