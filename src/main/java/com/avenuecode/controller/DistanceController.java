package com.avenuecode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.dto.DistanceDTO;
import com.avenuecode.dto.DistanciaNumberDTO;
import com.avenuecode.dto.InnerRouteDTO;
import com.avenuecode.model.Graph;
import com.avenuecode.repository.GraphRepository;
import com.avenuecode.service.DistanceService;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/distance")
public class DistanceController {

	private DistanceService distanceService = new DistanceService();
	
	@Autowired
	private GraphRepository graphRepository ;

	//4.Find distance for path on saved graph
	@PostMapping("/{id}")
	public DistanciaNumberDTO findDistancePath(@RequestBody DistanceDTO distance, @PathVariable Long id) throws Exception {
		Graph graph = graphRepository.findOne(id); 
		DistanciaNumberDTO dto = distanceService.getDistanceNumber(distance, graph);
		return dto;
	}

	//5.Find distance between two towns on saved graph
	@PostMapping("/{id}/from/{town1}/to/{town2}")
	public InnerRouteDTO distanceBeteenTown(@PathVariable Long id, @PathVariable String town1, @PathVariable String town2) {
		Graph graph = graphRepository.findOne(id);
		return distanceService.getShortestRoute(graph, town1, town2);
	}

}
