package com.avenuecode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.model.Graph;
import com.avenuecode.model.Route;
import com.avenuecode.repository.GraphRepository;
import com.avenuecode.repository.RouteRepository;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/graph")
public class GraphController {

	@Autowired
	private GraphRepository repository;

	@Autowired
	private RouteRepository routeRepository;

	// 1.Save graph configuration
	@PostMapping
	public Graph createGraph(@RequestBody Graph graph) {
		repository.save(graph);
		return graph;

	}

	// 2.Retrieve graph configuration
	@GetMapping("/{id}")
	public Graph getGraph(@PathVariable Long id) throws Exception {
		Graph graph = repository.findOne(id);
		if (graph != null) {
			return graph;
		} else {
			throw new Exception("NOT FOUND");
		}
	}

	public Graph getNewGraph() {
		Graph graph = new Graph();
		List<Route> routeList = (List<Route>) routeRepository.findAll();
		graph.setData(routeList);
		repository.save(graph);
		return graph;

	}

}
